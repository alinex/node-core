#!/usr/bin/env bats

@test "install: load lib" {
    run bin/install
    echo "$ bin/install\n$output"
    [ "$status" = 0 ]
    [[ $output =~ Done. ]]
    # lib installed
    [ -e "bin/lib/base.bash" ]
    # jq installed
    jq -V
}
 