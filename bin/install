#!/bin/bash

# load bash lib or use alternative log method
log() { echo $2; }
source_dir=$(dirname $(readlink -f "${BASH_SOURCE[0]:-$(pwd)/x}"))
if [ -e "$source_dir/lib/base.bash" ]; then
    source "$source_dir/lib/base.bash"
fi

log HEADING "Install bash commands"

log NOTICE "Update binaries"

# update bash lib
log INFO "Updating bash-lib..."
cd $source_dir
rm -rf lib
curl -s https://alinex.gitlab.io/bash-lib/downloads/bash-lib.tgz | tar -xz
mv dist "$source_dir/lib"
source "$source_dir/lib/base.bash"

log NOTICE "Install packages"

if ! command -v jq >/dev/null; then
    log INFO "Installing jq..."
    usesudo=$(usesudo)
    if command -v apk >/dev/null; then # alpine
        log_cmd $usesudo apk add jq
    elif command -v dnf >/dev/null; then # redhat
        log_cmd $usesudo dnf -y install jq
    elif command -v yum >/dev/null; then # redhat (old)
        log_cmd $usesudo yum -y install jq
    elif command -v pacman >/dev/null; then # arch
        log_cmd $usesudo pacman -Sy --noconfirm jq
    elif command -v apt-get >/dev/null; then # debian
        log_cmd $usesudo apt-get install -y jq
    elif command -v zypper >/dev/null; then # opensuse
        log_cmd $usesudo zypper install -y jq
    else 
        log ERROR "No supported package manager found!"
        log INFO "Please install jq by hand."
        exit 1
    fi
else
    log NOTICE "Tool jq already installed."
fi

log NOTICE Done.
