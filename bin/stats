#!/bin/bash

# load bash lib or use alternative log method
log() { echo $2; }
source_dir=$(dirname $(readlink -f "${BASH_SOURCE[0]:-$(pwd)/x}"))
if [ -e "$source_dir/lib/base.bash" ]; then
    source "$source_dir/lib/base.bash"
else
  # update bash lib
  log INFO "Install bash-lib..."
  rm -rf "$source_dir/lib"
  curl -s https://alinex.gitlab.io/bash-lib/downloads/bash-lib.tgz | tar -xz
  mv dist "$source_dir/lib"
  source "$source_dir/lib/base.bash"
fi

# -----------------------------------------------
# SETUP
# -----------------------------------------------
FILE=docs/assets/stats-pdf-license.txt

name=$(cat package.json 2>/dev/null | jq -r .name)
log HEADING "Information for $name"
echo "## Statistics" >$FILE
echo >>$FILE
gittag=$(git tag | sort -V | tail -1)
if [ -n "$gittag" ]; then
  echo "The following statistics (from Version $gittag) will give you a better understanding about it's complexity." >>$FILE
else
  echo "The following statistics will give you a better understanding about it's complexity." >>$FILE
fi
echo >>$FILE

# -----------------------------------------------
# Code
# -----------------------------------------------
if [ -d "src" ]; then
  echo "And this package contains the following code:">>$FILE
  echo >>$FILE
  loc=$(find src -type f -name \*.ts | xargs cat | wc -l)
  [ "$loc" -gt "0" ] && log NOTICE "Lines of TypeScript code: $loc"
  [ "$loc" -gt "0" ] && echo "- TypeScript $loc lines">>$FILE
  loc=$(find src -type f -name \*.js | xargs cat | wc -l)
  [ "$loc" -gt "0" ] && log NOTICE "Lines of JavaScript code: $loc"
  [ "$loc" -gt "0" ] && echo "- JavaScript $loc lines">>$FILE
  loc=$(find src -type f -name \*.svelte | xargs cat | wc -l)
  [ "$loc" -gt "0" ] && log NOTICE "Lines of Svelte code:     $loc"
  [ "$loc" -gt "0" ] && echo "- Svelte $loc lines">>$FILE
  echo >>$FILE
fi

# -----------------------------------------------
# Build
# -----------------------------------------------
npm run build >/dev/null 2>/dev/null
if [ $? -eq 0 ] && [ -e build ]; then
  size=$(du -sh build | sed 's/\s.*//' )
  files=$(find build -type f | wc -l)
  subpackages=$(npm ls --parseable | sed 's/.*\///' | sed 1d | sort | uniq | wc -l)
  log NOTICE "Build code:               $size in $files files ($subpackages packages)"
  echo -n "The build contains $files files with a size of $size including $subpackages packages. " >>$FILE
fi
npm run export >/dev/null 2>/dev/null
if [ $? -eq 0 ]; then
  size=$(du -sh build | sed 's/\s.*//')
  files=$(find build -type f | wc -l)
  log NOTICE "Exported site:            $size in $files files"
  echo -n "The export contains $files files with a size of $size. " >>$FILE
fi

# -----------------------------------------------
# JS Modules
# -----------------------------------------------
if [ -d "node_modules" ]; then
  subpackages=$(npm ls --parseable | sed 's/.*\///' | sed 1d | sort | uniq | wc -l)
  alinexpackages=$(npm ls --parseable | sed 's/@alinex\//@alinex/' | sed 's/.*\///' | sort | uniq | grep @alinex | wc -l)
  log NOTICE "Development Packages:     $subpackages including $alinexpackages Alinex packages"
  echo "The development version contains $subpackages packages (resolved) and $alinexpackages of it are from the Alinex project. " >>$FILE
fi

# -----------------------------------------------
# package.json
# -----------------------------------------------
if [ -e "package.json" ]; then
  version=$(npm view $name version 2>/dev/null)
  if [ $? -eq 0 ]; then
    log NOTICE "Latest Version:           $version"
    echo "The latest version in the repository is Version $version. " >>$FILE
    files=$(npm view $name dist.fileCount)
    size=$(npm view $name dist.unpackedSize | numfmt --to=si --suffix=B)
    log NOTICE "Last Release:             $files files -> $size uncompressed"
    echo "It has a size of $size in $files files. " >>$FILE
  else
    version=$(cat package.json | jq -r .version)
    log NOTICE "Development Version:      $version"
    echo "The latest version in the repository is Version $version. " >>$FILE
  fi
fi

# -----------------------------------------------
# Docker
# -----------------------------------------------
if [ -e Dockerfile ]; then
  image=$(grep org.label-schema.name Dockerfile | sed 's/"$//;s/.*"//')
  docker pull $image
  size=$(docker inspect -f "{{ .Size }}" $image | numfmt --to=si --suffix=B)
  layers=$(docker inspect -f '{{.RootFS.Layers}}' $image | wc -w)
  log NOTICE "Docker size:              ${size} in $layers layers"
  echo "The docker image is ${size} (uncompressed) in $layers layers. " >>$FILE
fi

# -----------------------------------------------
# Documentation
# -----------------------------------------------
docker run -v $(pwd):/data alinex/mkdocs
if [ -e site/*.pdf ]; then
  pages=$(pdfinfo site/*.pdf | grep Pages | awk '{print $2}')
  characters=$(find docs -type f -name \*.md | xargs cat | wc -m)
  images=$(find site -type f -exec file {} \; | grep -o -P '^.+: \w+ image' | wc -l)
  svg=$(find site -type f -name \*.html -exec cat {} \; | grep -c 'src="data:image/svg')
  images=$(($images + $svg)) 
  log NOTICE "Documentation:            $pages pages with $characters characters of markdown and $images images"
  echo "Documentation is $pages pages (in A4 PDF format) and contains $characters characters.">>$FILE
elif [ -e "docs" ]; then
  files=$(find docs -name \*.md | wc -l)
  characters=$(find docs -name \*.md | xargs cat | wc -m)
  images=$(find site -type f -exec file {} \; | grep -o -P '^.+: \w+ image' | wc -l)
  svg=$(find site -type f -name \*.html -exec cat {} \; | grep -c 'src="data:image/svg')
  images=$(($images + $svg)) 
  log NOTICE "Documentation:            $files files with $characters characters of markdown and $images images"
  echo "HTML Documentation has $files pages and contains $characters characters and $images images.">>$FILE
fi

# -----------------------------------------------
# Footer
# -----------------------------------------------

echo >>$FILE

if [ -e site/*.pdf ]; then
  echo "## Download Documentation" >>$FILE
  echo >>$FILE
  echo "If you want to have an offline access to the documentation, feel free to download the $pages pages [PDF Documentation]($(basename site/*.pdf))." >>$FILE
  echo >>$FILE
fi

[ -e LICENSE.md ] && cat LICENSE.md | sed 's/# /## /g' >>$FILE

log NOTICE Done.

exit

cd /home/alex/code
echo -n "Lines of code: "
find node-core/src node-async/src node-data/src node-datastore/src node-validator/src node-checkup/src node-server/src node-gui/src -type f -name \*.[tj]s | xargs cat | wc -l
echo -n "Pages of docs: "

( pdfinfo node-core/docs/*.pdf | grep Pages | awk '{print $2}' ) && ( pdfinfo node-async/docs/*.pdf | grep Pages | awk '{print $2}' ) && ( pdfinfo node-data/docs/*.pdf | grep Pages | awk '{print $2}' ) && ( pdfinfo node-datastore/docs/*.pdf | grep Pages | awk '{print $2}' ) && ( pdfinfo node-validator/docs/*.pdf | grep Pages | awk '{print $2}' ) && ( pdfinfo node-checkup/docs/*.pdf | grep Pages | awk '{print $2}' ) && ( pdfinfo node-server/docs/*.pdf | grep Pages | awk '{print $2}' ) && ( pdfinfo node-gui/docs/*.pdf | grep Pages | awk '{print $2}' )
