import { exit, ExitError } from './exit.js'
import logo from './logo.js'

export { logo, exit, ExitError }
export default {
  logo,
  exit,
  ExitError
}
