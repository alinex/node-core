import chalk from 'chalk'
import LPE from 'log-process-errors'

LPE() // init log-process-errors

/**
 * Error with additional information on how to exit.
 */
export class ExitError extends Error {
  /**
   * Detail information in addition to the base message.
   */
  description?: string;
  /**
   * Dynamic description method.
   */
  describe?: () => string;
  /**
   * Return code number to be used if exiting the process.
   */
  exit?: number;
  /**
   * NodeJS error code which defines the type of error.
   */
  code?: string;
  /**
   * Create error with additional exit information.
   * @param message default short message of error
   * @param description additional longer information
   * @param code return code to be send back from process
   */
  constructor(message: string, description?: string, code?: number | string) {
    super(message)
    this.description = description
    if (typeof code === 'string') this.code = code
    else if (code) this.exit = code
  }
}

const errCodes = new Map<string, number>(Object.entries({
  EACCES: 3,
  EADDRINUSE: 4,
  ECONNREFUSED: 6,
  ECONNRESET: 5,
  EEXIST: 3,
  EISDIR: 3,
  EMFILE: 3,
  ENOENT: 3,
  ENOTDIR: 3,
  ENOTEMPTY: 3,
  EPERM: 3,
  EPIPE: 3,
  ETIMEDOUT: 5
}))

/**
 * Exit process with given error or message.
 * @param err error message to be displayed
 * @param code specific exit code to send back from process
 */
export function exit(err: ExitError | string, code?: number | string | null) {
  if (typeof err === 'string') err = new ExitError(err)
  // autodetect code
  let rc: number = 0
  if (code == null || code === undefined) {
    if (err.exit != null && typeof err.exit === 'number') {
      rc = err.exit
    } else if (err.code != null && errCodes.has(err.code)) {
      rc = errCodes.get(err.code)!
    } else if (err) {
      rc = 1
    }
  } else if (typeof code === 'string') {
    rc = errCodes.get(code) || 1
  } else rc = 1
  // output error message
  if (err) {
    console.error(chalk.redBright('FAILED: ' + err.message.replace(/\n.*/s, '')))
    if (err.message.includes('\n')) { console.error(chalk.red(err.message.replace(/.*?\n/s, ''))) }
    if (err.stack) console.error(err.stack.slice(err.message.length + 7))
    if (err instanceof ExitError && err.description) { console.error(err.description) }
    if (typeof err.describe === 'function') console.error(err.describe())
  }
  process.exit(rc)
}

process.on('SIGINT', () => exit(new Error('Got SIGINT signal'), 130))
process.on('SIGTERM', () => exit(new Error('Got SIGTERM signal'), 143))
process.on('SIGHUP', () => exit(new Error('Got SIGHUP signal'), 129))
process.on('SIGQUIT', () => exit(new Error('Got SIGQUIT signal'), 131))
process.on('SIGABRT', () => exit(new Error('Got SIGABRT signal'), 134))

export default exit
