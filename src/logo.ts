// Logo
// =================================================
// A function to bring a common logo to all alinex cli interfaces using ASCII
// art can be made. The logo is designed to be output on console view.

import chalk from 'chalk'

/**
 * Generate specific colored ASCII art logo to be displayed on console.
 * @param title short title to be displayed below logo
 */
function logo(title: string = 'Application') {
  // get the title
  title = title
    .toUpperCase()
    .split('')
    .join(' ')
  title = ' '.repeat(Math.floor((80 - title.length) / 2)) + title
  // detect emblem function
  const sign = process.env.LOGO || 'alinex'
  const fn = <(title: string) => string>variant.get(sign) || variant.get('alinex')
  // return the logo
  return fn(title)
}

const variant = new Map<string, (title: string) => string>()

// Logo Definition
// -------------------------------------------------

variant.set('alinex', title => {
  const c1 = chalk.cyan
  const c2 = chalk.bold.yellow
  const ct = chalk.yellow
  const i = '  '
  return c1(`
  ${i}                         ${c2(' __   ____     __')}
  ${i}          ######  #####  ${c2('|  | |    \\   |  |  ')} ########### #####       #####
  ${i}         ######## #####  ${c2('|  | |     \\  |  |  ')}############  #####     #####
  ${i}        ######### #####  ${c2('|  | |  |\\  \\ |  |  ')}#####          #####   #####
  ${i}       ########## #####  ${c2('|  | |  | \\  \\|  |  ')}#####           ##### #####
  ${i}      ##### ##### #####  ${c2('|  | |  |_ \\     |  ')}############     #########
  ${i}     #####  ##### #####  ${c2('|  | |    \\ \\    |  ')}############     #########
  ${i}    #####   ##### #####  ${c2('|__| |_____\\ \\___|  ')}#####           ##### #####
  ${i}   #####    ##### #####                      #####          #####   #####
  ${i}  ##### ######### ########################## ############  #####     #####
  ${i} ##### ##########  ########################   ########### #####       #####
  ${i}____________________________________________________________________________

  ${ct(title)}
  ${i}____________________________________________________________________________

  `)
})

variant.set('divibib', title => {
  const c1 = chalk.bold.gray
  const c2 = chalk.green
  const ct = chalk.green
  const i = '  '
  return c1(`
  ${i}               ###   #                     #   ${c2('###           #   ###')}
  ${i}               ###  ###                   ###  ${c2('###          ###  ###')}
  ${i}               ###   #                     #   ${c2('###           #   ###')}
  ${i}               ###                             ${c2('###               ###')}
  ${i}${c2(' ## ')}      ########  ###  ###         ###  ###  ${c2('########     ###  ########')}
  ${i}${c2('####')}    ###    ###  ###   ###       ###   ###  ${c2('###    ###   ###  ###    ###')}
  ${i}${c2(' ## ')}   ###     ###  ###    ###     ###    ###  ${c2('###     ###  ###  ###     ###')}
  ${i}${c2('    ')}   ###     ###  ###     ###   ###     ###  ${c2('###     ###  ###  ###     ###')}
  ${i}${c2(' ## ')}   ###     ###  ###      ### ###      ###  ${c2('###     ###  ###  ###     ###')}
  ${i}${c2('####')}    ###    ###  ###       #####       ###  ${c2('###    ###   ###  ###    ###')}
  ${i}${c2(' ## ')}      ########  ###        ###        ###  ${c2('########     ###  ########')}
  ${i}____________________________________________________________________________

  ${ct(title)}
  ${i}____________________________________________________________________________
  `)
})

export default logo
