# Roadmap

!!! info

    I don't have a roadmap here, because this module will be extended any time a small method is seen as global to all my modules.

### Bugs

- [x] ~~_support debian/arch in update/install_~~ [2021-05-19]
- [ ] log-process-errors 6.3.0 ❯ 7.0.1 https://git.io/fhSGY fails with Unknown file extension ".ts" in mocha
      -> comes from TS module
- [ ] chalk 4.1.2 ❯ 5.0.0 https://github.com/chalk/chalk#readme

With both wait for typescript 4.6! https://github.com/microsoft/TypeScript/issues/46452

{!docs/assets/abbreviations.txt!}
