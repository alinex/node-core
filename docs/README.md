title: Overview

# Alinex Core

![core icon](https://assets.gitlab-static.net/uploads/-/system/project/avatar/11665107/bw.png){: .right .icon}

The core module is a base for all of my Alinex modules. It contains three parts:

1.  Some helper to **manage alinex modules** by myself and keep them in-sync like setup new modules or update the documentation theme.
2.  **Basic functions** needed in alinex modules. This is a collection of small functions which are not covered in separate modules.
3.  **Standards definition** for Alinex modules as documentation.

It's common methods which are necessary in nearly all of my packages at the moment contains:

- [exit](module/exit.md) - handler for process exits
- [logo](module/logo.md) - a method to present ASCII art command line logos

## Installation

To include this in your module install it:

```bash
npm install @alinex/core --save
```

Now only include it in your code and call it's methods. Or use the `bin/...` commands to use the Alinex environment helpers.

{!docs/assets/stats-pdf-license.txt!}

{!docs/assets/abbreviations.txt!}
