# Last Changes

## Version 1.9.0 (30.11.2021)

- added docker statistics
- restructured documentation stats

## Version 1.8.0 (17.11.2021)

- switch to use own docker for mkdocs
- reenable pdf build

## Version 1.7.0 (07.05.2021)

- upgrade to ES2019, Node >= 12
- added test coverage report

- Hotfix 1.7.1 (09.05.2021) - fix typescript settings
- Hotfix 1.7.2 (19.05.2021) - switch man page creation to ESM - support arch install
- Hotfix 1.7.3 (05.06.2021) - fix error output bug
- Hotfix 1.7.6 (10.11.2021) - optimize statistics calculation
- Hotfix 1.7.8 (11.11.2021) - further extend statistics

## Version 1.6.0 (01.01.2021)

- new doc theme
- update development packages

- Hotfix 1.6.1 (01.01.2021) - fix pdf link and optimize license inclusion
- Update 1.6.2 (02.01.2021) - add man page creation helper
- Hotfix 1.6.3 (03.01.2021) - also update bash modules
- Update 1.6.4 (06.05.2021) - update docs and eslint
- Update 1.6.5 (07.05.2021) - improved update script

## Version 1.5.0 (23.10.2020)

- Restructure documentation with abbreviations
- Add automatic generated statistics to docs

- Hotfix 1.5.1 (23.10.2020) - fix stats size calculation
- Hotfix 1.5.2 (23.10.2020) - fix counting only of different packages
- Hotfix 1.5.3 (23.10.2020) - fix alinex package counter
- Hotfix 1.5.4 (23.10.2020) - fix alinex package counter again

## Version 1.4.0 (15.02.2020)

Updating NPM scripts to better automate publishing.

- Version 1.4.1 (15.02.2020) - update modules
- Version 1.4.2 (08.03.2020) - update mkdocs settings
- Version 1.4.3 (08.03.2020) - fix PDF creation
- Version 1.4.4 (10.07.2020) - update modules
- Version 1.4.5 (26.09.2020) - better error reporting
- Version 1.4.6 (17.10.2020) - fix updating

## Version 1.3.0 (02.08.2019)

Remove async methods because now extracted into [alinex-async](https://alinex.gitlab.io/node-async).

- Version 1.3.1 (27.12.2019) - update modules

## Version 1.2.0 (30.06.2019)

Add async helper to work with promises in an easy way supporting: delay, retry, each, map, filter, parallel.

## Version 1.1.0 (17.04.2019)

- add ExitError as class

- Version 1.1.1 (25.04.2019) - adding optimized nodejs error output
- Version 1.1.2 (03.05.2019) - add doc comments

## Version 1.0.0 (08.04.2019)

- transform to TypeScript
- document new Standards
- adding bash update tool

- Version 1.0.1 (08.04.2019) - fix update script - add update documentation

## Version 0.4.1 (04.04.2019)

- add exit code handling

## Version 0.3.0 (04.04.2019)

- Transform into pure JavaScript (flow before)
- remove exit code handling

## Version 0.2.12 (28.06.2017)

- Fix divibib logo

## Version 0.2.11 (28.06.2017)

- Skip tests
- Add possibility for shutdown function on exit handler
- Fix documentation to hide style comments in github view.
- Updated ignore files.
- Update Travis.

## Version 0.2.10 (19.08.2016)

- Update alinex-builder@2.3.6, alinex-util@2.4.0, mocha@3.0.2
- Fix error code to be at least 1 if error given.
- Update docs.
- Update documentation.
- Fix logo documentation.
- Slightly updated documentation.
- Update documentation.
- Rename repository to use general alinex namespace naming.
- Add GitHub link.
- Rename index title.
- Updated documentation.
- Update docs.
- Add copyright sign.

## Version 0.2.9 (09.06.2016)

- Fix exit without error.

## Version 0.2.8 (09.06.2016)

- Fix exit without error.

## Version 0.2.7 (09.06.2016)

- Upgraded util, mocha and builder package.
- Allow calling exit without error.

## Version 0.2.6 (31.05.2016)

- Upgraded builder package.
- Optimize alinex logo.

## Version 0.2.5 (28.04.2016)

- Added optional exit code.
- Fix error code detection for undefined code or code -1.
- Fix changelog.

## Version 0.2.4 (27.05.2016)

- Add exit code auto detection.

## Version 0.2.3 (27.04.2016)

test new publish task

- Fixed tests to always have chalk enabled.
- Upgraded builder.
- Added missing util package.
- Merge with origin.
- Upgraded chalk package.

## Version 0.2.2 (22.04.2016)

- Fixed missing packages.

## Version 0.2.0 (22.04.2016)

- Added exit handling.

## Version 0.1.4 (29.02.2016)

- Change name because of NPM install problems.

## Version 0.1.3 (29.02.2016)

- Fixed tests.

## Version 0.1.2 (26.02.2016)

- Update version.
- Update version.

## Version 0.1.1 (26.02.2016)

- Update documentation.
- Upgraded builder, `chai` and `mocha`.
- Fixed changelog.

## Version 0.1.0 (26.02.2016)

- Added tests for logo display.
- Fixed package.json
- Restructure module to include logo code.

{!docs/assets/abbreviations.txt!}
