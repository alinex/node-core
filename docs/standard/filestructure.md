# File Structure

All modules in the Alinex namespace will use the same directory structure.
This follows the general standards and is described here.

## Overview

The locally installed system may be in one of the following states which
presents the development cycle of the system:

1.  **Source** - the real code base
2.  **Development** - after installing
3.  **Build** - if installed from npm or after building
4.  **Productive** - after the system is configured

Each of these states may have some of the possible directories so they will
be referenced in the further description.

### Source

> The developer will start with the GIT source by cloning or forking.

### Development

> While in development sometimes additional directories will be created while compiling and
> testing the code.

### Build

> For productive use, this is the start point. You get a ready to run compiled
> system.

### Productive

> In the first run the system may be configured and create some additional
> directories for configurations and runtime data.

## Possible directories

The following list displays all directories of any state which may exist each
listed with the states to which it belongs:

```coffee
.             # source
bin           # all
data          # all
docs          # source, development
etc           # all
lib           # (source, development) build, production
node_modules  # development, ...
src           # source, development
test          # source, development
var           # production data
```

Read the further sections to get more information of what resides in which
directory and how it is used and created.

## Ignoring Files

To properly support the file structure in all phases two ignore files are needed:

- `.gitignore` (used to not push everything to git)
- `.npmignore` (used fro npm publishing)

## Directories

### Source Stage

The source specifies what is stored in the code repository.

This stage contains the following directories:

```coffee
bin           # executable files
data          # base data
  locals      # i18n localization
etc           # default/example configuration
docs          # general documentation as gitbook
src           # source code
test          # test data and test suites
  data        # test data
  mocha       # mocha test suites
```

The source code resides in the `src` folder and will be copied/compiled into
`lib` to run. This step is done on build of package.

### Development Stage

Shows what the developer will find on his machine while developing and testing
the system. While testing the development system will also get all the
directories from productive which are not listed here.

This stage contains the following directories:

```coffee hl_lines="5 6"
bin           # executable files
data          # base data
docs          # created documentation (optional)
etc           # default/example configuration
lib           # copied/compiled code
node_modules  # npm installed packages
src           # source code
test          # test data and test suites
```

### Installed Stage

This is what you get after a fresh npm installation.

This stage contains the following directories:

```coffee hl_lines="5"
bin           # executable files
data          # base data
etc           # configurations
lib           # copied/compiled code
var           # data and code which maybe changed in installation
node_modules  # npm installed packages
```

### Productive Stage

And finally this shows what resides on the productive server.

This stage contains the following directories:

```coffee hl_lines="6 7 8"
bin           # executable files
data          # base data
etc           # configurations
lib           # copied/compiled code
var           # data and code which maybe changed in installation
  config      # compiled configuration
  data        # persistent file store
  log         # log files
node_modules  # npm installed packages
```

## Where what belongs to

The following list should give an overview of there to store what:

- cache files -> systems temp folder
- configuration -> system or user `alinex` folder or `etc`
- resources for binaries -> `bin/lib`
- temporary files -> systems temp folder

{!docs/assets/abbreviations.txt!}
