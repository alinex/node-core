title: TypeScript

# TypeScript Language

Most modules will be written in TypeScript but be compiled into pure JavaScript with additional TypeScript definitions for runtime. That means it is also usable if you are not familiar with typescript.

The decision to use typescript is based on:

- type safety for more reliable code
- it is a superset of javascript
- great IDE support

At the moment typescript is configured to:

- compile to ES2018, but it may be changed anytime
- using commonJS module syntax, maybe changing to ES2016 later
- TypeScript declarations included
- sourceMaps are generated
- comments are removed
- strict type checking is enabled
- unused locals are not allowed

## package.json

To support typescript you should add the following:

=== "package.json"

    ```json
    "scripts": {
        "build": "rm -rf lib && tsc",
        "prepare": "npm run build",
        "test": "mocha test/mocha/**.js --exit"
    }
    ```

## JSDoc

To further help developers in the IDE, all public elements of a package should be documented using JSDoc.

{!docs/assets/abbreviations.txt!}
