# Executables

If your package contains executable files like bash scripts to start from CLI this is best supported as follows.

## File Structure

The relevant files are:

```coffee
bin/
    my-script       # script with x-flag calling cli.js
    my-script.1     # man file
lib/
    cli.js          # compiled start file
src/
    my-script.1.md  # source for man file
    cli.ts          # typescript source
package.js          # package dependencies
```

## Usage

Add the script to build the man pages, add them to the build script and reference the resulting file as man page:

=== "package.json"

    ```json
    "scripts": {
        "man": "alinex-man src/my-script.1.md",
        "build": "npm run man && rm -rf lib && tsc",
        "prepare": "npm run build"
    },
    "man": [
        "./bin/my-script.1"
    ]
    ```

Now the real binary should be called using NodeJS in the she-bang and it's only purpose is to load the `lib/cli.js` and let it work with one of those scripts:

=== "JavaScript"

    ```js
    #!/usr/bin/env node

    require(`${__dirname}/../lib/cli.js`) // eslint-disable-line import/no-dynamic-require
    ```

=== "Bash"

    ```bash
    #!/bin/bash

    source_dir=$(dirname $(readlink -f "${BASH_SOURCE[0]:-$(pwd)/x}"))
    /usr/bin/env node "$(dirname $source_dir)/lib/cli.js"
    ```

    The a bit complex looking part is to detect the script directory and call `cli.js` from there.

{!docs/assets/abbreviations.txt!}
