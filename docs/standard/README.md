title: Overview

# Alinex JavaScript Standards

This part describes my current standards in coding JavaScript modules. They are followed not only in this small module, but in all my modules.

!!! warning

    Keep in mind that older versions are using older no longer here documented standards.

    Also it isn't a must but a should rule so small differences to this standards are allowed.

The basic settings used for my new modules are:

| Software                                               | Comment              |
| ------------------------------------------------------ | -------------------- |
| Node >= 10                                             | Allows to use ES2018 |
| [`typescript`](https://alinex.gitlab.io/js/typescript) | TypeScript           |
| `debug` + `chalk`                                      | Debugging on demand  |
| `mocha`                                                | To run unit tests    |
| [`mkdocs`](https://alinex.gitlab.io/env/mkdocs)        | Documentation        |

To work with this standards some small bash tools are included here.

## Install Helper

!!! attention

    You first have to create your package.json if not already done, use `npm init` to do this interactively.

To use this helpers you have to install them, which will check your system and load necessary libraries:

```bash
npm install @alinex/core --save
node_modules/@alinex/core/bin/install
```

## Update to Standard

Afterwards you can update your project anytime by calling:

```bash
node_modules/@alinex/core/bin/update
```

This will install missing files in an initial version and update some which should be always up to date.
The processing depends on the package type detected by the first part in the repository name.

### Code in NodeJS

For packages with name `node-...`

-   load `.gitignore` if not present from @alinex/core
-   load `.gitlab-ci.yml` if not present from @alinex/core
-   load `.npmignore` if not present from @alinex/core
-   add mocha and typescript modules
-   load `tsconfig.json` if not present from @alinex/core

### Documentation Setup

This will setup the `mkdocs` documentation to be corresponding to [alinex.gitlab.io](https://alinex.gitlab.io):

-   install `mkdocs` if not already done
-   load `docs/assets` if not present from [alinex.gitlab.io](https://alinex.gitlab.io)
-   load `docs/assets/extra.css` from [alinex.gitlab.io](https://alinex.gitlab.io)
-   load `docs/assets/pdfmathjax.js` from [alinex.gitlab.io](https://alinex.gitlab.io)
-   load `docs/assets/default.jpg` from [alinex.gitlab.io](https://alinex.gitlab.io)
-   load `docs/assets/abbreviations.txt` from [alinex.gitlab.io](https://alinex.gitlab.io)
-   load `docs/manifest.webmanifest` from @alinex/core
-   load `mkdocs.yml` if not present from @alinex/core
-   load `.markdownlint.json` from @alinex/core
-   load `docs/policy.md` if not present from @alinex/core
-   write `docs/README.md` if not present
-   write `docs/CHANGELOG.md` if not present
-   write `docs/roadmap.md` if not present
-   load `LICENSE.md` if not present from @alinex/core

## NPM Scripts

Mostly the following scripts are defined to be called:

-   `npm run man` - this will create the man page file for binary
-   `npm run stats` - output statistics and update include file for documentation
-   `npm run build` - compile typescript
-   `npm run prepare` - call build before publishing
-   `npm run test` - run the unit tests
-   `preversion` - run tests before bumping version
-   `postversion` - publish,
-   `postpublish` - update stats, push to git origin

With the above scripts publishing is as easy as calling `npm version patch`, `npm version minor` or `npm version major` and npm as well as git is updated.

{!docs/assets/abbreviations.txt!}
