title: Overview

# Alinex Core Module

The core module is a base for all of my alinex modules. It has some common methods which are necessary in nearly any of my packages.

At the moment it contains:

-   [exit](exit.md) - handler for process exits
-   [logo](logo.md) - a method to present ASCII art command line logos

## Installation

To include this in your module install it:

```bash
npm install @alinex/core --save
```

Now only include it in your code and call it's methods.

## Usage

Command line modules should import the default methods and can use the [logo](logo.md) display from it. But for module you may also only import specific functionalities:

=== "TypeScript"

    ```ts
    // load default methods
    import core from '@alinex/core'
    // load all parts
    import * as core from '@alinex/core'
    // load specific elements into own variables
    import { logo } from '@alinex/core'
    ```

=== "JavaScript"

    ```js
    // load only the defaults
    const core = require('@alinex/core').default
    // load all parts
    const core = require('@alinex/core')
    ```

{!docs/assets/abbreviations.txt!}
