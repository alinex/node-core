# Exit Handler

See [exit codes](https://alinex.gitlab.io/concepts/exitcodes/) description for a detailed information about exit codes and the standards for them.

## Error display

The error output of NodeJS is optimized to display better readable and colorful error messages. You will get this by only including this module once.

## Interrupt Signals

This comes in two parts. At first a handler will be set which will work on interrupt signals and exit the process with a short message. This will be automatically done by loading the module:

=== "TypeScript"

    ```ts
    import '@alinex/core'
    ```

=== "JavaScript"

    ```js
    require('@alinex/core');
    ```

Supported signals are:

| Interrupt | Signal    | Exit Code |
| --------- | --------- | --------- |
| 1         | `SIGHUP`  | 129       |
| 2         | `SIGINT`  | 130       |
| 3         | `SIGQUIT` | 131       |
| 6         | `SIGABRT` | 134       |
| 9         | `SIGTERM` | 143       |

Not all signals are supported here, they are mapped to 128 + signal number for the exit code.

## Managing Uncaught Errors

In NodeJS there may be some errors which were thrown and neither catched. This can be from rejected promises or thrown exceptions. The following code at the start of your program will catch them all and send them to the exit handler to make a nice error message and exit.

```ts
import core from '@alinex/core';

process.on('uncaughtException', err => core.exit(err, 1));
process.on('unhandledRejection', (err: any) => core.exit(err, 1));
```

## Controlled Exit

Use this to manually exit the process with a possible error.

!!! definition

    ```ts
    core.exit(
        err: ExitError | Error | string,
        code: number | string | null
    )
    ```

It is possible to use the `exit` method for controlled exit:

=== "TypeScript"

    ```ts
    import core from '@alinex/core'

    // exit with code 100
    core.exit(new Error("Command already running, can't be called in parallel!"), 100);
    ```

=== "JavaScript"

    ```js
    const core = require('@alinex/core');

    // exit with code 100
    core.exit(new Error("Command already running, can't be called in parallel!"), 100);
    ```

This will:

-    output the `error.message`
-    output a possible `error.description` property
-    auto set the code if possible
-    exit the process with the given code

The alinex tools are based on the bash exit codes 0, 1 and 124-143. In addition the
codes 3-6 are used for NodeJS system codes and some alinex codes are set in
the range 16-120 like:

| Code | Description                                 |
| ---: | ------------------------------------------- |
|    0 | OK - no error                               |
|    1 | General error which should not occur        |
|    2 | Command parameter problem                   |
|    3 | File system access problem                  |
|    4 | Network problems                            |
|    5 | Service or system access problem            |
|    6 | No such service or address                  |
|  124 | command times out                           |
|  125 | if a command itself fails                   |
|  126 | Command invoked cannot execute              |
|  127 | "command not found"                         |
|  128 | Invalid argument to exit                    |
|  129 | `SIGHUP` (Signal 1)                         |
|  130 | `SIGINT` like through ++ctrl+c++ (Signal 2) |
|  131 | `SIGQUIT` (Signal 3)                        |
|  134 | `SIGABRT` or `SIGIOT` (Signal 6)            |
|  143 | `SIGTERM` (Signal 15)                       |
|  255 | Exit status out of range                    |

## ExitError

!!! definition

    ```ts
    class ExitError extends Error {
        constructor(
            message: string,
            description?: string,
            code?: number | string
        )
        description?: string
        exit?: number
        code?: string
    }
    ```

Such errors are used to auto detect the exit code and display a more detailed error message in case of exiting the system with this. Therefore the `exit()` method explicitly supports the additional fields of this type of Error.

=== "TypeScript"

    ```ts
    import { exit, ExitError } from '@alinex/core'

    const err = new ExitError('Wrong Parameter', 'Only the following parameters are allowed...')
    err.exit = 2
    exit(err);
    ```

=== "JavaScript"

    ```js
    const core = require('@alinex/core');

    const err = new core.ExitError('Wrong Parameter', 'Only the following parameters are allowed...')
    err.exit = 2
    core.exit(err);
    ```

The exit code will be taken from:

-   the `err.exit` field
-   the `err.code` name (auto detect for node errors)

This procedure gives you the possibility to define the exit code then the error
occurs and throw it first but decide later on other position if you want to give
it to the exit handler.

{!docs/assets/abbreviations.txt!}
