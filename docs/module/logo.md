# Logo

Display an ASCII art logo on console.

!!! definition

    ```ts
    core.logo(title: string): string
    ```

This is often used at startup of command line calls:

=== "ES Modules"

    ```ts
    import core from '@alinex/core'

    // output logo on console
    console.log(core.logo('Code Documentation Extractor'));
    ```

=== "CommonJS"

    ```js
    const core = require('@alinex/core');

    // output logo on console
    // eslint-disable-next-line no-console
    console.log(core.logo('Code Documentation Extractor'));
    ```

This will give a logo like:

![logo](logo.png)

For some special custom developments alternative logos are available, which can be selected by environment setting `LOGO`.

{!docs/assets/abbreviations.txt!}
