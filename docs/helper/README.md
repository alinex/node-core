title: Helper

# Alinex Environment Tools

## Install

The bash script under `node_modules/@alinex/core/bin/install` will initialize a new module to the alinex structure and add a lot of settings.

## Update

The bash script under `node_modules/@alinex/core/bin/update` will update some of the settings from the core documentation or the @alinex/core files.

!!! Note

    You can also call it with `node_modules/.bin/alinex-update`.

## Stats

The bash script under `node_modules/@alinex/core/bin/stats` will show and update some statistic data which can be included in the documentation (see it on the bottom of [main page](../README.md)).


!!! Example

    You should include it with the following scripts in `package.json`:

    ```json
    "stats": "node_modules/.bin/alinex-stats",
    "postpublish": "npm run stats && git add -A && git commit -m \"Update documentation\"; git push origin --all && git push origin --tags"
    ```

    Now you may update the stats using `npm run stats` and it will be done after the next publication automatically.

### Man Pages

Some of my NodeJS scripts are callable from CLI on Linux systems. Therefore I also support man pages, but to make it easy I write them in markdown, too. The following script will generate new man pages in the `bin` folder out of the given markdown file:

!!! Example

    You should include it with the following scripts in `package.json`:

    ```json
    "man": "node_modules/.bin/alinex-man src/datastore.1.md",
    "build": "npm run man && rm -rf lib && tsc",
    ```

    This will automatically update the man page on each build run.

{!docs/assets/abbreviations.txt!}
