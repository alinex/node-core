## Statistics

The following statistics (from Version v1.11.0) will give you a better understanding about it's complexity.

And this package contains the following code:

- TypeScript 182 lines

The development version contains 26 packages (resolved) and 0 of it are from the Alinex project. 
The latest version in the repository is Version 1.11.0. 
It has a size of 34KB in 20 files. 
HTML Documentation has 13 pages and contains 31064 characters and 0 images.

## License

### Alinex Core

> Copyright 2016, 2019 - 2023 Alexander Schilling (<https://gitlab.com/alinex/node-core)>

!!! abstract "Apache License, Version 2.0"

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
