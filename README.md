# Alinex Core

The core module is a base for all of my Alinex modules. It has some common methods which are necessary in nearly any of my packages.

At the moment it contains:

-   Logo - a method to present ASCII art command line logos

A complete help with examples can be found at [alinex.gitlab.io/node-core](https://alinex.gitlab.io/node-core).

## Usage

To include this in your module install it:

```bash
npm install @alinex/core --save
```

Now only include it in your code and call it's methods:

```ts
import core from '@alinex/core'; 

// init signal exit handler
core.init();

// output logo on console
// eslint-disable-next-line no-console
console.log(core.logo('Portal Application'));

// exit with code and message
core.exit(100, new Error("Command already running, can't be called in parallel!"));
```

## Update TS/JS

Update packages:

```
npm install
npm run test
npm-check -u
npm audit fix
npm audit fix --force # if possible and needed
```

Update `package.json`:

- `"type": "module",`
- `"test": "find test/mocha -name '*.js' | xargs mocha --exit",`
- `"node": ">=20"`
 
Update `tsconfig.json`:

- `"target": "ES2022"`
- `"module": "NodeNext"`
- `//"moduleResolution": "node"`
 
Add `.mocharc.json`:

```json
{
    "loader": "ts-node/esm"
}
```

Fixes in `src`:

- local imports using extension
- use chalk colors through default export

Fixes in `test`:

- ts -> js
- local imports using extension

Build and Test

```
npm run test
npm run build
```

Finish and update:

- copyright update
- git stage changes
- `npm version minor`

## Documentation

Find a complete manual under [alinex.gitlab.io/node-core](https://alinex.gitlab.io/node-core).

If you want to have an offline access to the documentation, feel free to download the [PDF Documentation](https://alinex.gitlab.io/node-core/alinex-core.pdf).

## License

(C) Copyright 2016, 2019 - 2023 Alexander Schilling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> <https://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
